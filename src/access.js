/**
 * 权限控制器
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 */
export default function access(initialState) {
  const { currentUser } = initialState ?? {};
  return {
    canAdmin: currentUser && (currentUser.access === 'admin' || currentUser.access === 'super_admin'),
    canSupperAdmin: currentUser && currentUser.access === 'super_admin',
  };
}
