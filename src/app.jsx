import { SettingDrawer } from '@ant-design/pro-layout';
import { PageLoading } from '@ant-design/pro-layout';
import { history, Link } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
import { currentUser as queryCurrentUser } from './services/ant-design-pro/api';
import { getUserInfo as getUserInfo } from './services/onepiece/onepiece-server';
import defaultSettings from '../config/defaultSettings';
import { lang } from 'moment';
import initialState from '@/.umi/plugin-initial-state/models/initialState';

const isDev = process.env.NODE_ENV === 'development';
const loginPath = '/user/login';
let token = localStorage.getItem("token");

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

// 统一请求拦截器-请求前拦截
const authHeaderInterceptor = (url, options) => {
  const o = options;

  // 如果是Oauth2授权时，会将token携带到请求url中，然后从请求url中获取token，并再本地缓存中保存下来
  const requestUrl = window.location.href;
  var arr = requestUrl.split("token=");
  if (arr[1]){
    token = arr[1];
    localStorage.setItem("token",token)
  }

  // headers 里面的内容是和后端协商好的
  if (history.location.pathname !== loginPath) {
    o.headers = {
      // ...options.headers,
      'Authentication-Token': localStorage.getItem('token'),
    };
    return {
      options: o
    }
  }
  return {
    options:o
  }
}

// 统一异常处理拦截器，参考文章：https://blog.csdn.net/qq_40319394/article/details/123524519
// https://umijs.org/zh-CN/plugins/plugin-request
const errorHandler = (error) => {
  const { response } = error;
  if (response && response.status) {
    console.log(error);
  }
  return response;
};

export const request = {
  // 新增手动添加的错误异常拦截器
  errorHandler,
  // 新增自动添加的Token请求前拦截器
  requestInterceptors: [authHeaderInterceptor],
};

/**
 * @see https://umijs.org/zh-CN/plugins/plugin-initial-state
 */
export async function getInitialState() {

  // 全局获取登录用户信息
  const fetchUserInfo = async () => {
    try {
      const result = await getUserInfo(token);
      return result.data;
    } catch (error) {
      history.push(loginPath);
    }

    return undefined;
  };

  // 如果不是登录页面，执行
  if (history.location.pathname !== loginPath) {
    // 获取登录用户基本信息
    const currentUser = await fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
      settings: defaultSettings,
    };
  }

  // 如果是登录页面，执行
  if (history.location.pathname === loginPath) {
    // 获取登录用户基本信息
    const result = await getUserInfo(token);
    if (result.success){
      // 说明已经登录，跳转到首页即可
      window.location.href = '/admin/home'
    }
  }

  return {
    fetchUserInfo,
    settings: defaultSettings,
  };
}

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout = ({ initialState, setInitialState }) => {
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState?.currentUser?.name,
    },
    footerRender: () => <Footer />,
    onPageChange: () => {
      const { location } = history; // 如果没有登录，重定向到 login

      if (!initialState?.currentUser && location.pathname !== loginPath) {
        history.push(loginPath);
      }
    },
    links: isDev
      ? [
          // <Link key="openapi" to="/umi/plugin/openapi" target="_blank">
          //   <LinkOutlined />
          //   <span>OpenAPI 文档</span>
          // </Link>,
          // <Link to="/~docs" key="docs">
          //   <BookOutlined />
          //   <span>业务组件文档</span>
          // </Link>,
        ]
      : [],
    menuHeaderRender: undefined,
    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    // 增加一个 loading 的状态
    childrenRender: (children, props) => {
      // if (initialState?.loading) return <PageLoading />;
      return (
        <>
          {children}
          {!props.location?.pathname?.includes('/login') && (
            <SettingDrawer
              disableUrlParams
              enableDarkTheme
              settings={initialState?.settings}
              onSettingChange={(settings) => {
                setInitialState((preInitialState) => ({ ...preInitialState, settings }));
              }}
            />
          )}
        </>
      );
    },
    ...initialState?.settings,
  };
};
